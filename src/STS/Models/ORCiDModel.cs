﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.STS.Models;

public class ORCiDModel
{
    [Required]
    [Display(Name = "ORCiD_JWT")]
    public string ORCiD_JWT { get; set; }
}
