﻿using System.Collections.Generic;

namespace Coscine.Api.STS.Models;

public class LoginUrls
{
    public string OrcidUrl { get; set; } = null!;
    public IEnumerable<Sustainsys.Saml2.Metadata.EntityId> IdentityProviders { get; set; } = null!;
}
