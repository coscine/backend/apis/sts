﻿using Coscine.ApiCommons;
using Coscine.Configuration;
using System.Text;

namespace Coscine.Api.STS
{
    public class Program : AbstractProgram<ConsulConfiguration>
    {
        public static string HostUrl = Configuration.GetStringAndWait("coscine/local/api/additional/url");
            // Default: $"https://{Dns.GetHostName()}.{System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName}";
        public static string ApiPath = $"/coscine/api/{((System.Reflection.Assembly.GetEntryAssembly() != null) ? System.Reflection.Assembly.GetEntryAssembly().GetName().Name : System.Reflection.Assembly.GetExecutingAssembly().GetName().Name)}";

        public static string MainUrl {
            get
            {
                return HostUrl + ApiPath; 
            }
        }

        public static void Main()
        {
            Configuration.PutAndWait("coscine/global/sts/url", Encoding.ASCII.GetBytes(MainUrl));
            InitializeWebService<Startup>();
        }
    }
}