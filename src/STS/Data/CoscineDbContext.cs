﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.STS.Data
{
    public class CoscineDbContext : IdentityDbContext<CoscineUser>
    {
        public CoscineDbContext(DbContextOptions<CoscineDbContext> options)
               : base(options)
        {
        }
    }
}
