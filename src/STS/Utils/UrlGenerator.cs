﻿using Coscine.JwtHandler;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Coscine.Api.STS.Utils
{
    /// <summary>
    /// URL Generator Helper methods
    /// </summary>
    public class UrlGenerator
    {
        /// <summary>
        /// Login URL for Coscine
        /// </summary>
        /// <param name="request">HttpRequest from the context</param>
        /// <returns>Complete login URL for the host machine</returns>
        public static string GetLoginUrl(HttpRequest request)
        {
            var query = request.Query;
            var addedEntries = "";
            foreach (var queryEntry in query)
            {
                addedEntries += "&" + queryEntry.Key + "=" + queryEntry.Value;
            }
            var loginUrl = Program.HostUrl + "/login/";
            return loginUrl;
        }

        /// <summary>
        /// Terms of Service URL for Coscine
        /// </summary>
        /// <param name="request">HttpRequest from the context</param>
        /// <returns></returns>
        public static string GetTOSUrl(HttpRequest request)
        {
            var queryString = GetLoginUrl(request);
            return queryString + "tos";
        }

        public static string ExtendReturnUrl(string returnUrl, HttpRequest request)
        {
            string retString = returnUrl ?? "";
            if (request.Query["redirect"].Count != 0 && !retString.Contains("redirect="))
            {
                if (!retString.Contains("?"))
                {
                    retString += "?";
                }
                else
                {
                    retString += "&";
                }
                retString += "redirect=" + request.Query["redirect"][0];
            }
            return retString;
        }

        public static string ORCiDRedirectUrl()
        {
            return Program.MainUrl + "/orcid/login?returnUrl=" + Program.MainUrl;
        }

        public static string ShibbolethRedirectUrl()
        {
            return Program.MainUrl + "/shibboleth/login?returnUrl=" + Program.MainUrl;
        }

        public static string MergeCallbackRedirectUrl()
        {
            return Program.MainUrl + "/merge/callback?returnUrl=" + Program.MainUrl;
        }
    }
}