﻿using Coscine.Api.STS.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Json;

namespace Coscine.Api.STS.Utils
{
    public class ORCiDHandler
    {
        public static readonly string _loginStatusUrl = "https://orcid.org/userStatus.json";

        public static string GetORCiDOAuthUrl()
        {
            return Program.Configuration.GetStringAndWait("coscine/global/orcid/url")
                .Replace("{client_id}", Program.Configuration.GetStringAndWait("coscine/global/orcid/clientid"));
        }

        public static ClaimsPrincipal VerifiyORCiDJWT(string jwt)
        {
            var th = new JwtSecurityTokenHandler();
            var webKeyJson = new WebClient().DownloadString(Program.Configuration.GetStringAndWait("coscine/global/orcid/jwksurl"));
            webKeyJson = webKeyJson.Substring(webKeyJson.IndexOf("[") + 1);
            webKeyJson = webKeyJson.Substring(0, webKeyJson.LastIndexOf("]"));
            var jsonWebKey = new JsonWebKey(webKeyJson);

            var validationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateLifetime = false,
                ValidateIssuer = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = jsonWebKey,
                ValidIssuers = new[] { Program.Configuration.GetStringAndWait("coscine/global/orcid/issuer") }
            };
            return th.ValidateToken(jwt, validationParameters, out _);
        }
    }
}
