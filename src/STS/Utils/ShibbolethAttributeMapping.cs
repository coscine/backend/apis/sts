﻿using Coscine.Database.DataModel;
using System.Collections.Generic;
using System.Security.Claims;

namespace Coscine.Api.STS.Utils
{
    public class ShibbolethAttributeMapping
    {
        public static string Identifier { get; private set; } = "urn:oid:1.3.6.1.4.1.5923.1.1.1.10";
        public static string PairwiseID { get; private set; } = "urn:oasis:names:tc:SAML:attribute:pairwise-id";

        public static Dictionary<string, string> LabelMapping { get; private set; } = new Dictionary<string, string>()
        {
            { "urn:oid:1.3.6.1.4.1.5923.1.1.1.10", "eduPersonTargetedId" },
            { "urn:oasis:names:tc:SAML:attribute:pairwise-id", "pairwise-id" },
            { "urn:oid:2.16.840.1.113730.3.1.241", "DisplayName" },
            { "urn:oid:2.5.4.4", "Surname" },
            { "urn:oid:1.3.6.1.4.1.5540.2.1.96", "RWTHGivenname" },
            { "urn:oid:2.5.4.42", "Givenname" },
            { "urn:oid:0.9.2342.19200300.100.1.3", "EmailAddress" },
            { "urn:oid:1.3.6.1.4.1.5923.1.1.1.9", "ScopedAffiliation" },
            { "urn:oid:2.5.4.10", "Organization" },
        };

        public static void SetUserAttribute(User user, string identifier, object value)
        {
            switch (identifier)
            {
                case "urn:oid:2.16.840.1.113730.3.1.241":
                    user.DisplayName = (string)value;
                    break;
                case "urn:oid:2.5.4.4":
                    user.Surname = (string)value;
                    break;
                case "urn:oid:1.3.6.1.4.1.5540.2.1.96":
                    user.Givenname = (string)value;
                    break;
                case "urn:oid:2.5.4.42":
                    user.Givenname = (string)value;
                    break;
                case "urn:oid:0.9.2342.19200300.100.1.3":
                    user.EmailAddress = (string)value;
                    break;
                case "urn:oid:1.3.6.1.4.1.5923.1.1.1.9":
                    if (user.Entitlement == null || !(user.Entitlement.Contains("employee") || user.Entitlement.Contains("staff")))
                    {
                        user.Entitlement = (string)value;
                    }
                    break;
                case "urn:oid:2.5.4.10":
                    user.Organization = (string)value;
                    break;
                default:
                    break;
            }
        }

        public static User CreateUser(ClaimsPrincipal principal)
        {
            User user = new User();
            foreach(var key in LabelMapping.Keys)
            {
                SetUserAttribute(user, key, principal.FindFirstValue(key));
            }
            if (user.DisplayName == null)
            {
                user.DisplayName = user.Givenname + " " + user.Surname;
            }
            return user;
        }

    }
}
