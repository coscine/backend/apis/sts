﻿using Coscine.Api.STS.Data;
using Coscine.Api.STS.Utils;
using Coscine.ApiCommons;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NSwag;
using NSwag.Generation.Processors.Security;
using Sustainsys.Saml2;
using Sustainsys.Saml2.AspNetCore2;
using Sustainsys.Saml2.Metadata;
using Sustainsys.Saml2.Metadata.Services;
using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace Coscine.Api.STS
{
    public class Startup : AbstractDefaultStartup
    {
        /// <summary>
        /// In this static variable the CurrentSaml2Options are stored,
        /// so that the IdentityProviders and other entries can be accessed.
        /// </summary>
        public static Saml2Options CurrentSaml2Options;

        private ApplicationInformation _applicationInformation;

        public Startup()
        {
        }

        public override void SetBasePath(ApplicationInformation applicationInformation)
        {
            base.SetBasePath(applicationInformation);
            _applicationInformation = applicationInformation;
        }

        public override void ConfigureServicesExtensionLate(IServiceCollection services)
        {
            base.ConfigureServicesExtensionLate(services);

            CryptoConfig.AddAlgorithm(typeof(AesGcmAlgorithm), AesGcmAlgorithm.AesGcm128Identifier);

            services.AddDbContext<CoscineDbContext>(
                options =>
                    options.UseInMemoryDatabase("CoscineDbContext")
            );

            services.AddIdentity<CoscineUser, IdentityRole>()
                .AddEntityFrameworkStores<CoscineDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie()
                .AddSaml2(options =>
                {
                    CurrentSaml2Options = options;

                    options.SPOptions.EntityId = new EntityId(
                        Program.Configuration.GetStringAndWait(
                            "coscine/global/shibboleth/entityid",
                            Program.HostUrl + "/Shibboleth"
                        )
                    );
                    options.SPOptions.PublicOrigin = new Uri(Program.HostUrl);
                    options.SPOptions.ModulePath = Program.ApiPath + "/Saml2";

                    options.SPOptions.DiscoveryServiceUrl = new Uri(Program.Configuration.GetStringAndWait(
                        "coscine/global/shibboleth/federation/discoveryserviceurl"
                    ));

                    options.SPOptions.Compatibility.UnpackEntitiesDescriptorInIdentityProviderMetadata = true;
                    new Federation(Program.Configuration.GetStringAndWait("coscine/global/shibboleth/federation/metadata"), true, options);

                    var contact = new ContactPerson
                    {
                        Type = ContactType.Support,
                    };
                    contact.EmailAddresses.Add(Program.Configuration.GetStringAndWait("coscine/global/shibboleth/contact"));
                    options.SPOptions.Contacts.Add(contact);

                    var attributeConsumingService = new AttributeConsumingService
                    {
                        IsDefault = true,
                        ServiceNames = { new LocalizedName("Saml2", "en") }
                    };

                    foreach (var pair in ShibbolethAttributeMapping.LabelMapping)
                    {
                        attributeConsumingService.RequestedAttributes.Add(
                            new RequestedAttribute(pair.Key)
                            {
                                FriendlyName = pair.Value,
                                IsRequired = true,
                                NameFormat = RequestedAttribute.AttributeNameFormatUri
                            });
                    }

                    options.SPOptions.AttributeConsumingServices.Add(attributeConsumingService);

                    var pfx = Program.Configuration.GetAndWait("coscine/global/sts/pfx");
                    var passwordString = Program.Configuration.GetStringAndWait("coscine/global/sts/pfxpassword");

                    var x509Certificate2 = new X509Certificate2(pfx, passwordString);

                    options.SPOptions.ServiceCertificates.Add(x509Certificate2);
                    options.SPOptions.WantAssertionsSigned = true;

                    // For dealing with `'DeclarationReference' must be an absolute Uri``
                    options.SPOptions.Compatibility.IgnoreAuthenticationContextInResponse = true;
                });

            // Register the Swagger services
            services.AddOpenApiDocument((settings) =>
            {
                settings.Title = _applicationInformation.AppName;
                settings.Version = _applicationInformation.Version.ToString();
                settings.PostProcess = (postProcess) =>
                {
                    postProcess.Host = _applicationInformation.ApiUrl;
                };
                settings.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT token"));
                settings.DocumentProcessors.Add(new SecurityDefinitionAppender("JWT token", new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    Description = "Copy 'Bearer ' + valid JWT token into field",
                    In = OpenApiSecurityApiKeyLocation.Header
                }));
            });
        }

        public override void ConfigureExtensionLate(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.ConfigureExtensionLate(app, env);

            app.UseStaticFiles();
            app.UseOpenApi();
            app.UseSwaggerUi3();
        }
    }
}