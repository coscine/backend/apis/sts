﻿using Coscine.Api.STS.Data;
using Coscine.Api.STS.Models;
using Coscine.Api.STS.Utils;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Coscine.Api.STS.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<CoscineUser> _signInManager;

        public AccountController(SignInManager<CoscineUser> signInManager)
        {
            _signInManager = signInManager;
        }

        /// <summary>
        /// Return relevant URLs for logging the user in
        /// </summary>
        /// <returns>Return relevant URLs for logging the user in</returns>
        [HttpGet("[controller]/urls/login")]
        public ActionResult<LoginUrls> GetLoginUrls()
        {
            return new LoginUrls
            {
                OrcidUrl = ORCiDHandler.GetORCiDOAuthUrl() + UrlGenerator.ORCiDRedirectUrl(),
                IdentityProviders = Startup.CurrentSaml2Options?.IdentityProviders.KnownIdentityProviders.Select((idp) => idp.EntityId)
            };
        }

        /// <summary>
        /// Route for logging the user out of Coscine
        /// </summary>
        /// <returns>Redirects the user back to the login page</returns>
        [Route("[controller]/logout")]
        public async Task<ActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            ViewBag.LoginUrl = UrlGenerator.GetLoginUrl(Request) + "?logout=true";

            return Redirect(ViewBag.LoginUrl);
        }
    }
}