﻿using Coscine.ActiveDirectory;
using Coscine.Api.STS.Utils;
using Coscine.ApiCommons;
using Coscine.Database.Models;
using Coscine.JwtHandler;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;

namespace Coscine.Api.STS.Controllers
{
    public class HomeController : Controller
    {
        private static readonly HttpClient _httpClient = new();

        /// <summary>
        /// The controller that the user is automatically entering when accesing the route .../coscine/api/Coscine.STS/
        /// </summary>
        /// <returns>Redirecting</returns>
        [Route("/")]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var userIdString = User.Identity.Name;
                var userId = new Guid(userIdString);

                ProcessSignIn(User);

                // Process where the user will be redirected to
                if (!AreTOSAccepted(userId))
                {
                    // Will redirect to the ToS page
                    ViewBag.Redirect = UrlGenerator.GetTOSUrl(Request);
                }
                else
                {
                    var redirects = Request.Query["redirect"];
                    if (redirects.Count > 0)
                    {
                        // Will redirect to the desired redirect location
                        ViewBag.Redirect = redirects[0];
                    }
                    else
                    {
                        // Will redirect to root/home page
                        ViewBag.Redirect = "/";
                    }
                    if (string.IsNullOrWhiteSpace(ViewBag.Redirect))
                    {
                        ViewBag.Redirect = "/";
                    }
                }

                var dictionary = new Dictionary<string, string>
                {
                    { "UserId", userId.ToString() }
                };
                var jwtHandler = new JWTHandler(Program.Configuration);
                ViewBag.Token = jwtHandler.GenerateJwtToken(dictionary);

                // STS Exit Point
                return View(); // Adds token to local storage and redirects
            }
            string loginUrl = UrlGenerator.GetLoginUrl(Request);
            return Redirect(loginUrl);
        }

        private static bool AreTOSAccepted(Guid userId)
        {
            var tosModel = new TOSModel();
            var tosAcceptedList = tosModel.GetAllWhere((entry) => entry.UserId == userId);
            var currentTos = Configurator.Configuration.GetStringAndWait("coscine/global/tos/version");
            return tosAcceptedList != null
                && tosAcceptedList.Any((entry) => entry.Version == currentTos);
        }

        private static void ProcessSignIn(ClaimsPrincipal user)
        {
            var userModel = new UserModel();
            var realUser = userModel.GetById(Guid.Parse(user.Identity.Name));

            ADHandler.AddUser(realUser, Program.Configuration);
        }
    }
}
