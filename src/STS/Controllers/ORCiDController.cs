﻿using Coscine.Api.STS.Data;
using Coscine.Api.STS.Models;
using Coscine.Api.STS.Utils;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

#region DupFinder Exclusion
namespace Coscine.Api.STS.Controllers
{
    public class ORCiDController : Controller
    {
        private readonly SignInManager<CoscineUser> _signInManager;

        public ORCiDController(SignInManager<CoscineUser> signInManager)
        {
            _signInManager = signInManager;
        }

        /// <summary>
        /// ORCiD Login Route
        /// </summary>
        /// <param name="returnUrl">URL to be redirected to</param>
        /// <returns>Route that is necessary to parse the hashed ORCiD data</returns>
        [Route("[controller]/login")]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = UrlGenerator.ExtendReturnUrl(returnUrl, Request);
            return View();
        }

        /// <summary>
        /// Execute ORCiD validation and login
        /// </summary>
        /// <param name="model">ORCiD Model after successful parsing</param>
        /// <param name="returnUrl">URL to be redirected to</param>
        /// <returns></returns>
        [HttpPost("[controller]/login")]
        public async Task<ActionResult> Login(ORCiDModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var claimsPrincipal = ORCiDHandler.VerifiyORCiDJWT(model.ORCiD_JWT);
                string surname = "";
                string givenname = "";
                string ORCiD = "";
                foreach (var claim in claimsPrincipal.Claims)
                {
                    if(claim.Type == ClaimTypes.NameIdentifier)
                    {
                        ORCiD = claim.Value;
                    }
                    else if(claim.Type == ClaimTypes.Surname)
                    {
                        surname = claim.Value;
                    }
                    else if(claim.Type == ClaimTypes.GivenName)
                    {
                        givenname = claim.Value;
                    }
                }

                var externalAuthenticatorModel = new ExternalAuthenticatorModel();
                var orcidAuthItem = externalAuthenticatorModel.GetWhere((externalAuthenticator) => externalAuthenticator.DisplayName == "ORCiD");

                var externalIdModel = new ExternalIdModel();
                var mapping = externalIdModel.GetAllWhere((map) => map.ExternalId1 == ORCiD && map.ExternalAuthenticatorId == orcidAuthItem.Id);
                var userModel = new UserModel();
                User user;
                if (mapping.Any())
                {
                    var userId = mapping.First().UserId;
                    user = userModel.GetById(userId);
                }
                else
                {
                    user = new User
                    {
                        DisplayName = (givenname + " " + surname).Trim(),
                        Surname = surname,
                        Givenname = givenname
                    };
                    userModel.Insert(user);
                    externalIdModel.Insert(new ExternalId
                    {
                        ExternalId1 = ORCiD,
                        ExternalAuthenticatorId = orcidAuthItem.Id,
                        Organization = "https://orcid.org/",
                        UserId = user.Id
                    });
                }

                var coscineUser = new CoscineUser()
                {
                    UserName = user.Id.ToString(),
                    Email = user.EmailAddress ?? ""
                };

                var result = await _signInManager.UserManager.CreateAsync(coscineUser);
                await _signInManager.SignInAsync(coscineUser, isPersistent: false);

                return Redirect(UrlGenerator.ExtendReturnUrl(returnUrl, Request));
            }
            ViewBag.ReturnUrl = UrlGenerator.ExtendReturnUrl(returnUrl, Request);
            return View();
        }
    }
}
#endregion